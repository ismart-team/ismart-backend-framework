<?php

namespace Ismart\BackendFramework;

use Ismart\BackendFramework\Controllers\AuthController;
use Ismart\BackendFramework\Controllers\AdminController;
use Ismart\BackendFramework\Controllers\PageController;
use Ismart\BackendFramework\Controllers\FormController;
use Ismart\BackendFramework\Middlewares\StoreMiddleware;
use Ismart\BackendFramework\Middlewares\QueryParamsMiddleware;
use Ismart\BackendFramework\Middlewares\CSRFKeysMiddleware;
use Ismart\BackendFramework\Middlewares\ValidationMiddleware;
use Ismart\BackendFramework\Middlewares\JsonBodyParserMiddleware;
use Tuupola\Middleware\JwtAuthentication;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Slim\Csrf\Guard;
use Psr\Container\ContainerInterface as Container;
use Exception;

class App
{
    private $app;

    private $routes = [
        "home" => [
            'path' => '/',
            'method' => 'get',
            'controller' => PageController::class,
        ],

        "form" => [
            'path' => '/form/',
            'method' => 'get',
            'controller' => FormController::class,
            'validation' => [
                'name' => [
                    'required' => true,
                    'alnum' => true,
                    'length' => [3, 7]
                ],
                'age' => [
                    'tel' => true,
                ]
            ]
        ],
    ];

    /**
     * @param  array  $options
     */
    public function __construct(array $options)
    {
        $this->mergeOptions($options);
        $container = $this->createContainer($options['rootPath'], $options['users'], $options['secret']);
        $this->addMiddlewares($container, $options['rootPath'], $options['users'], $options['secret']);
        $this->addCSRF($container);
        $this->defineRoutes();
        $this->defineAdminRoutes();
        $this->defineAuthRoutes();

        // Set the cache file for the routes. Note that you have to delete this file
        // whenever you change the routes.
        $this->app->getRouteCollector()->setCacheFile(
            $options['rootPath'] . '/cache/routes.cache'
        );

    }

    /**
     * @param  array  $options
     */
    public function mergeOptions(array $options)
    {
        if (array_key_exists('routes', $options)) {
            $this->routes = array_replace_recursive($this->routes, $options['routes']);
        }
    }

    /**
     * @param  string  $rootPath
     * @param  array   $users
     * @param  string  $secret
     *
     * @return Container
     */
    public function createContainer(string $rootPath, $users, $secret)
    {
        $controllersNames = array_map(function ($route) {
            return $route['controller'];
        }, array_values($this->routes));

        $preferences = new Preferences($rootPath);

        try {
            $container = ContainerFactory::create($preferences, array_unique($controllersNames), $users, $secret);
        } catch (Exception $e) {
            die($e->getMessage());
        }


        // Set the container to create the App with AppFactory.
        AppFactory::setContainer($container);
        $this->app = AppFactory::create();

        return $container;
    }

    /**
     * @param  Container  $container
     */
    public function addCSRF(Container $container)
    {
        $responseFactory = $this->app->getResponseFactory();

        $container->set('csrf', function () use ($responseFactory) {
            return new Guard($responseFactory);
        });
    }


    /**
     * @param  Route  $route
     * @param  Route  $routePreview
     * @param  array  $validations
     */
    private function addValidations($route, $routePreview, $validations)
    {
        $route->add(new ValidationMiddleware($validations));
        $routePreview->add(new ValidationMiddleware($validations));
    }

    /**
     * @param  Container  $container
     * @param  string     $rootPath
     * @param  array      $users
     * @parsm  string     $dataDir
     */
    public function addMiddlewares(Container $container, string $rootPath, $users, $secret)
    {
        $this->app->add(new StoreMiddleware($container->get('store')));
        $this->app->add(new QueryParamsMiddleware());
        $this->app->add(new CSRFKeysMiddleware());
        $this->app->add(new JsonBodyParserMiddleware());

        $this->app->addRoutingMiddleware();

        $twig = new Twig(
            [
                $rootPath . '/templates',
                __DIR__ .'/templates'
            ],
            [
                'cache' => $rootPath . '/cache',
                'auto_reload' => true,
                'debug' => false,
            ]
        );
        $this->app->add(
            new TwigMiddleware(
                $twig,
                $container,
                $this->app->getRouteCollector()->getRouteParser(),
                $this->app->getBasePath()
            )
        );

        $displayErrorDetails = true;
        $logErrors = true;
        $logErrorDetails = false;
        $this->app->addErrorMiddleware($displayErrorDetails, $logErrors, $logErrorDetails);

        $this->app->add(new JwtAuthentication([
            "secure" => false,
            "path" => "/admin/",
            "secret" => $secret
        ]));
    }

    public function defineRoutes()
    {
        foreach ($this->routes as $name => $routeParams) {
            $method = $routeParams['method'];
            $route = $this->app->$method($routeParams['path'], $routeParams['controller'])->setName($name)->add('csrf');;
            $route_preview = $this->app->$method($routeParams['path'] . 'preview/', $routeParams['controller'])->setName($name . '_preview')->add('csrf');;

            if (array_key_exists('middlewares', $routeParams)) {
                $this->addMiddlewaresToRoute($route, $route_preview, $routeParams['middlewares']);
            }

            if (array_key_exists('validation', $routeParams)) {
                $this->addValidations($route, $route_preview, $routeParams['validation']);
            }

        }


    }

    public function defineAdminRoutes()
    {
        $this->app->get('/admin/', AdminController::class)->setName('admin');
        $this->app->get('/admin/pages/', AdminController::class . ':getAllPages')->setName('adminPages');
        $this->app->get('/admin/pages/{name}/', AdminController::class . ':getPage')->setName('adminPage');
        $this->app->post('/admin/pages/{name}/', AdminController::class . ':writePreviewPage')->setName('adminPageWrite');
        $this->app->put('/admin/pages/{name}/', AdminController::class . ':copyPreviewPateToProductionPage')->setName('adminPagePublish');
    }

    private function defineAuthRoutes()
    {
        $this->app->post('/login/', AuthController::class . ':login')->setName('login');
    }

    /**
     * @param  Route  $route
     * @param  Route  $routePreview
     * @param  array  $middlewares
     */
    public function addMiddlewaresToRoute($route, $routePreview, $middlewares)
    {
        foreach ($middlewares as $middleware) {
            $route->add($middleware);
            $routePreview->add($middleware);
        }
    }

    public function run()
    {
        $this->app->run();
    }
}
