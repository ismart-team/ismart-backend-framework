<?php

declare(strict_types=1);

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\JsonFormatter;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return [
    'logger' => function (ContainerInterface $container): LoggerInterface {
        // Get the preferences from the container.
        $preferences = $container->get('preferences');

        // Instantiate a new logger and push a handler into the logger.
        $logger = new Logger('app');
        $stream_handler = new StreamHandler($preferences->getRootPath() . '/logs/app.json');
        $stream_handler->setFormatter(new JsonFormatter());
        $logger->pushHandler($stream_handler);

        return $logger;
    },
];
