<?php

declare(strict_types=1);

use  Ismart\BackendFramework\Controllers\AdminController;
use Psr\Container\ContainerInterface;

return [
    AdminController::class => function (ContainerInterface $container): AdminController {
        return new AdminController(
            $container->get('view'),
            $container->get('store'),
            $container->get('preferences'),
            $container->get('logger')
        );
    },
];
