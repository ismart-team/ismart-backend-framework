<?php


namespace Ismart\BackendFramework;

use Ismart\BackendFramework\Controllers\AuthController;
use Ismart\BackendFramework\utils\Store;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

class ContainerFactory
{
    /**
     * @param object $preferences
     * @param array  $controllersNames
     * @param array  $users
     * @param string $secret
     *
     * @return ContainerInterface
     * @throws \Exception
     */
    public static function create(object $preferences, array $controllersNames, $users, $secret): ContainerInterface
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
            'preferences' => $preferences,
        ]);

        $containerBuilder->addDefinitions([
            'store' => new Store($preferences->getRootPath() . '/data')
        ]);

        $controllers = array_reduce($controllersNames, function ($acc, $item) {
            $acc[$item] = function (ContainerInterface $container) use ($item) {
              return new $item(
                  $container->get('view'),
                  $container->get('preferences'),
                  $container->get('logger'),
                  $container
              );
            };
            return $acc;
        }, []);

        $containerBuilder->addDefinitions([
            AuthController::class => function () use ($users, $secret) {
                return new AuthController($users, $secret);
            }
        ]);

        $containerBuilder->addDefinitions(__DIR__ . '/config/container-definitions.php');
        $containerBuilder->addDefinitions(__DIR__ . '/config/container-admin-controllers.php');

        $containerBuilder->addDefinitions($controllers);

        return $containerBuilder->build();
    }
}
