<?php

namespace Ismart\BackendFramework\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

final class TelException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Не прошло валидацию',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'Негатив',
        ],
    ];
}
