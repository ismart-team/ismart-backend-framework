<?php

namespace Ismart\BackendFramework\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;

class CSRFKeysMiddleware
{
    private $nameKey = 'csrf_name';
    private $valueKey = 'csrf_value';

    /**
     * Store middleware invokable class
     *
     * @param  Request  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $data = $request->getAttribute('data') ?: [];

        $data['csrf'] = [
            'nameKey' => $this->nameKey,
            'valueKey' => $this->valueKey,
            'name' => $request->getAttribute($this->nameKey),
            'value' => $request->getAttribute($this->valueKey)
        ];

        $request = $request->withAttribute('data', $data);
        return $handler->handle($request);
    }

}
