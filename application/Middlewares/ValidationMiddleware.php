<?php

namespace Ismart\BackendFramework\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Rules;
use Ismart\BackendFramework\Validation\Rules as CustomRules;
use Respect\Validation\Factory as ValidatorsFactory;
use Respect\Validation\Exceptions\NestedValidationException;

/**
 * Validation for Slim.
 */
class ValidationMiddleware
{
    /**
     * Validators.
     *
     * @var array
     */
    protected $validators = [];

    /**
     * Options.
     *
     * @var array
     */
    protected $options = [
    ];

    /**
     * The translator to use fro the exception message.
     *
     * @var callable
     */
    protected $translator = null;

    /**
     * Errors from the validation.
     *
     * @var array
     */
    protected $errors = [];

    /**
     * The 'errors' attribute name.
     *
     * @var string
     */
    protected $errors_name = 'errors';

    /**
     * The 'has_error' attribute name.
     *
     * @var string
     */
    protected $has_errors_name = 'has_errors';

    /**
     * The 'validators' attribute name.
     *
     * @var string
     */
    protected $validators_name = 'validators';

    /**
     * The 'translator' attribute name.
     *
     * @var string
     */
    protected $translator_name = 'translator';

    /**
     * Create new Validator service provider.
     *
     * @param null|array             $fields
     * @param null|callable          $translator
     * @param []|array               $options
     */
    public function __construct($fields = null, $translator = null, $options = [])
    {
//        $this->addCustomValidators();

        if (is_array($fields)) {
            $this->setValidators($fields);
        }
        $this->translator = $translator;
        $this->options = array_merge($this->options, $options);
    }

    /**
     * Validation middleware invokable class.
     *
     * @param  Request  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $this->errors = [];
        $params = $request->getQueryParams();
        $params = array_merge((array) $request->getAttribute('routeInfo')[2], $params);
        $this->validate($params, $this->validators);

        $request = $request->withAttribute($this->errors_name, $this->getErrors());
        $request = $request->withAttribute($this->has_errors_name, $this->hasErrors());
        $request = $request->withAttribute($this->validators_name, $this->getValidators());
        $request = $request->withAttribute($this->translator_name, $this->getTranslator());

        return $handler->handle($request);
    }

    private function addCustomValidators()
    {
        ValidatorsFactory::setDefaultInstance(
            (new ValidatorsFactory())
                ->withRuleNamespace('Ismart\\BackendFramework\\Validation\\Rules')
                ->withExceptionNamespace('Ismart\\BackendFramework\\Validation\\Exceptions')
        );
    }

    private function setValidators($fields)
    {
        $this->validators = array_reduce(array_keys($fields), function ($acc, $name) use ($fields) {

            $rules = $fields[$name];

            $fieldValidatorRules = array_map(function ($ruleName, $ruleParams) {
                return $this->createValidator($ruleName, $ruleParams);
            }, array_keys($rules), array_values($rules));

            $acc[$name]= new Rules\AllOf($fieldValidatorRules);

            return $acc;

        }, []);

//        $usernameValidator = v::alnum()->noWhitespace()->length(1, 10);
//        $ageValidator = v::numeric()->positive()->between(1, 20);
//
//        var_dump($this->validators);
//
//        var_dump($ageValidator);
//
//        $this->validators = array(
//            'name' => $usernameValidator,
//            'age' => $ageValidator
//        );
    }

    private function createValidator($name, $params)
    {

        if ($params !== false) {
            switch ($name) {
                case 'required':
                    return new Rules\NotEmpty();
                case 'alnum':
                    return new Rules\Alnum();
                case 'length':
                    return new Rules\Length(...$params);
                case 'tel':
                    return new CustomRules\Tel();
                default:
                    break;
            }
        }
    }

    /**
     * Validate the parameters by the given params, validators and actual keys.
     * This method populates the $errors attribute.
     *
     * @param array $params     The array of parameters.
     * @param array $validators The array of validators.
     * @param array $actualKeys An array that will save all the keys of the tree to retrieve the correct value.
     */
    private function validate($params = [], $validators = [], $actualKeys = [])
    {
        //Validate every parameters in the validators array
        foreach ($validators as $key => $validator) {
            $actualKeys[] = $key;
            $param = $this->getNestedParam($params, $actualKeys);
            if (is_array($validator)) {
//                var_dump($params, $validator, $actualKeys);
                $this->validate($params, $validator, $actualKeys);
            } else {
                try {

                    $validator->assert($param);
                } catch (NestedValidationException $exception) {
                    if ($this->translator) {
                        $exception->setParam('translator', $this->translator);
                    }
                    $this->errors[implode('.', $actualKeys)] = $exception->getMessages();
                }
            }

            //Remove the key added in this foreach
            array_pop($actualKeys);
        }
    }

    /**
     * Get the nested parameter value.
     *
     * @param array $params An array that represents the values of the parameters.
     * @param array $keys   An array that represents the tree of keys to use.
     *
     * @return mixed The nested parameter value by the given params and tree of keys.
     */
    private function getNestedParam($params = [], $keys = [])
    {
        if (empty($keys)) {
            return $params;
        } else {
            $firstKey = array_shift($keys);
            if ($this->isArrayLike($params) && array_key_exists($firstKey, $params)) {
                $params = (array) $params;
                $paramValue = $params[$firstKey];

                return $this->getNestedParam($paramValue, $keys);
            } else {
                return false;
            }
        }
    }

    /**
     * Check if the given $params is an array like variable.
     *
     * @param array $params The variable to check.
     *
     * @return boolean Returns true if the given $params parameter is array like.
     */
    private function isArrayLike($params)
    {
        return is_array($params) || $params instanceof \SimpleXMLElement;
    }

    /**
     * Check if there are any errors.
     *
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * Get errors.
     *
     * @return array The errors array.
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get validators.
     *
     * @return array The validators array.
     */
    public function getValidators()
    {
        return $this->validators;
    }


    /**
     * Get translator.
     *
     * @return callable The translator.
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Set translator.
     *
     * @param callable $translator The translator.
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }
}
