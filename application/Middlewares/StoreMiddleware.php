<?php

namespace Ismart\BackendFramework\Middlewares;

use Slim\Routing\RouteContext;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;

class StoreMiddleware
{
    private $store;

    public function __construct($store)
    {
        $this->store = $store;
    }

    /**
     * Store middleware invokable class
     *
     * @param  Request  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $route_name = $route->getName();

        $data = $request->getAttribute('data') ?: [];

        $data['data'] = $this->store->getData($route_name);

        $request = $request->withAttribute('data', $data);

        return $handler->handle($request);
    }
}
