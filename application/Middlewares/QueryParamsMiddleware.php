<?php

namespace Ismart\BackendFramework\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;

class QueryParamsMiddleware
{
    /**
     * Store middleware invokable class
     *
     * @param  Request  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $data = $request->getAttribute('data') ?: [];

        $data['query_params'] = $request->getQueryParams();

        $request = $request->withAttribute('data', $data);
        return $handler->handle($request);
    }

}
