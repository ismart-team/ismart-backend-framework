<?php

declare(strict_types=1);

namespace Ismart\BackendFramework\Controllers;

/**
 * This abstract class defines methods and properties used by all controllers.
 *
 * @package App\Controllers
 */
abstract class AbstractController
{
}
