<?php

declare(strict_types=1);

namespace Ismart\BackendFramework\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

class PageController extends AbstractTwigController
{
    /**
     * @var Preferences
     */
    private $preferences;

    /**
     * HomeController constructor.
     *
     * @param Twig        $twig
     * @param $preferences
     */
    public function __construct(Twig $twig, $preferences, LoggerInterface $logger)
    {
        parent::__construct($twig);

        $this->preferences = $preferences;
        $this->logger = $logger;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args = []): Response
    {
        $data = $request->getAttribute('data');

        $template_name = $data['data']['template'];

//        var_dump($request->getAttribute('csrf_name'));
//        $this->logger->info('This is log');

        return $this->render($response, $template_name . '.twig', $data);
    }
}
