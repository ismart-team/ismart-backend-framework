<?php

namespace Ismart\BackendFramework\Controllers;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthController
{
    private $users;
    private $secret;

    public function __construct($users, $secret)
    {
        $this->users = $users;
        $this->secret = $secret;
    }

    public function login(Request $request, Response $response, array $args = []): Response
    {
        $name = null;
        $password = null;
        $body = null;

        $contentType = $request->getHeaderLine('Content-Type');

        if (strstr($contentType, 'application/json')) {
            $data = json_decode(file_get_contents('php://input'), true);
            $name = $data['name'];
            $password = $data['password'];
        }

        $user = $this->findUser($name);

        if ($user === false) {
            $body = 'User not found';
        } else {
            if (password_verify($password, $user['password'])) {
                $token = JWT::encode(['name' => $name], $this->secret);
                $body = $token;
            } else {
                $body = 'Wrong password';
            }
        }

        $response->getBody()->write($body);

        return $response->withHeader('Content-Type', 'application/json');

    }

    public function findUser($name)
    {

        $user = array_filter($this->users, function ($item) use ($name) {
            return $item['name'] === $name;
        });


        if (is_array($user) && !empty($user)) {
            return $user[0];
        }

        return false;
    }
}
