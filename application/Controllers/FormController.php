<?php

declare(strict_types=1);

namespace Ismart\BackendFramework\Controllers;

use Ismart\BackendFramework\Preferences;
use Ismart\BackendFramework\utils\Mail;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class FormController extends AbstractTwigController
{
    /**
     * @var Preferences
     */
    private $preferences;

    /**
     * HomeController constructor.
     *
     * @param Twig        $twig
     * @param Preferences $preferences
     */
    public function __construct(Twig $twig, Preferences $preferences, $logger, $responseFactory)
    {
        parent::__construct($twig);

        $this->preferences = $preferences;

//        echo('<pre>');
//        var_dump($responseFactory);
//        echo('</pre>');
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args = []): Response
    {
        $data= $request->getAttribute('data');

        $body = $this->render($response, 'email/email.twig', $data['query_params'])->getBody();

//        var_dump($body);

        $provider = $data['data']['provider'];
        $email = $data['data']['email'];
        $password = $data['data']['password'];
        $recepients = $data['data']['recepients'];

        if($request->getAttribute('has_errors')){
            //There are errors, read them
            $errors = $request->getAttribute('errors');
            var_dump($errors);
        } else {
            $mailer = new Mail($provider, $email, $password, $recepients);

            $mailer->send($body, $data['query_params']);
        }

        $responseClass = get_class($response);
        $newResponse = new $responseClass();
        $newBody = $newResponse->getBody();
        $newBody->write('New body');

        return $response->withBody($newBody);
    }
}
