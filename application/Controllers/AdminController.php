<?php

declare(strict_types=1);

namespace Ismart\BackendFramework\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;

class AdminController extends AbstractTwigController
{

    private $preferences;
    private $store;

    /**
     * HomeController constructor.
     *
     * @param Twig        $twig
     * @param  $preferences
     */
    public function __construct(Twig $twig, $store, $preferences, LoggerInterface $logger)
    {
        parent::__construct($twig);

        $this->store = $store;
        $this->preferences = $preferences;
        $this->logger = $logger;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args = []): Response
    {
        return $this->render($response, 'admin.twig');
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function getAllPages(Request $request, Response $response, array $args = []): Response
    {
        $files = $this->store->getList('preview', 'string');
        $response->getBody()->write($files);

        return $response->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function getPage(Request $request, Response $response, array $args = []): Response
    {
        $name = $args['name'];
        $data = $this->store->getData($name . '_preview', 'string');
        $response->getBody()->write($data);

        return $response->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function writePreviewPage(Request $request, Response $response, array $args = []): Response
    {

        $name = $args['name'];
        $parsedBody = $request->getParsedBody();

        $result = $this->store->writeData($name . '_preview', $parsedBody);
        $response->getBody()->write((string)$result);

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function copyPreviewPateToProductionPage(Request $request, Response $response, array $args = []): Response
    {
        $name = $args['name'];
        $data = $this->store->getData($name . '_preview');

        $result = $this->store->writeData($name, $data);
        $response->getBody()->write((string)$result);

        return $response->withHeader('Content-Type', 'application/json');

    }

}
