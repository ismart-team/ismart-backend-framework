<?php

namespace Ismart\BackendFramework\utils;

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    /**
     * @param string  $provider - name smtp server
     * @param string  $email
     * @param string  $password
     * @param array   $recepients
     *
     */
    public function __construct(string $provider, string $email, string $password, array $recepients)
    {
        $this->mail = new PHPMailer();

        $smtpSettings = $this->getSMTPSettings($provider);

        $this->mail->IsSMTP();
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPSecure = $smtpSettings['secure'];
        $this->mail->Host = $smtpSettings['host'];
        $this->mail->Port = $smtpSettings['port'];
        $this->mail->Username = $email;
        $this->mail->Password = $password;

        $this->mail->CharSet = "utf-8";

        $this->mail->SetFrom($email, $_SERVER["HTTP_HOST"]);
        $this->addAddress($recepients);
    }

    /**
     * @param array   $recepients
     *
     */
    private function addAddress($recepients)
    {
        foreach ($recepients as $recepient) {
            $this->mail->AddAddress($recepient, "");
        }
    }

    /**
     * @param array   $attaches
     */
    private function addAttachment($attaches)
    {
        foreach ($attaches as $attach) {
            $this->mail->addAttachment($attach);
        }
    }

    /**
     * @param string   $provider
     *
     * @return array
     */
    private function getSMTPSettings($provider)
    {
        switch ($provider) {
            case "yandex":
                $settings = [
                    'secure' => 'ssl',
                    'host' => 'smtp.yandex.ru',
                    'port' => 465,
                ];
                break;
            case "gmail":
            default:
            $settings = [
                'secure' => 'tls',
                'host' => 'smtp.gmail.com',
                'port' => 587,
            ];
        }

        return $settings;
    }

    /**
     * @param string   $body
     * @param array    $form_data
     *
     * @return boolean
     */
    public function send($body, $form_data)
    {
        $this->mail->MsgHTML($body);

        if (array_key_exists('attaches', $form_data)) {
            $this->addAttachment($form_data);
        }

        $this->mail->Subject = $form_data["subject"];
        return $this->mail->send();
    }
}
