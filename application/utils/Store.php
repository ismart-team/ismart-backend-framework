<?php

namespace Ismart\BackendFramework\utils;

class Store
{
    private $rootPath;

    public function __construct($rootPath)
    {
        $this->rootPath = $rootPath;
    }

    public function getData(String $routeName, String $type = 'array')
    {
        $full_path = $this->rootPath . '/' . $this->getPathByRouteName($routeName);

        if (!file_exists($full_path)) {
            return false;
        }

        $data = file_get_contents($full_path);

        if ($type === 'string') {
            return $data;
        }

        return json_decode($data, true);
    }

    public function getList(String $dir, $type = 'array')
    {
        $full_path = $this->rootPath . '/' . $dir;

        if (!is_dir($full_path)) {
            return false;
        }

        $files = array_slice(scandir($full_path), 2);

        if ($type === 'string') {
            return json_encode($files, JSON_UNESCAPED_UNICODE);
        }

        return $files;
    }

    public function writeData(String $routeName, $data) :bool
    {
        $full_path = $this->rootPath . '/' . $this->getPathByRouteName($routeName);

        if (!file_exists($full_path)) {
            return false;
        }

        if (is_array($data)) {
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        }

        return file_put_contents($full_path, $data);
    }

    private function getPathByRouteName(string $routeName) :string
    {
        $is_preview = mb_substr($routeName, -8) === '_preview';
        $name = explode('_', $routeName)[0];

        return ($is_preview ? 'preview' : 'production') . '/' . $name . '.json';
    }
}
